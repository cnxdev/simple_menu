import 'dart:ui';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/logs.dart';

const defaultLocale = Locale('th', 'TH');

const String prefSelectedLanguageCode = "SelectedLanguageCode";
const String prefSelectedCountryCode = "SelectedCountryCode";


setLocale(String languageCode, String countryCode) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  printLog("Preference Updated $languageCode");
  prefs.setString(prefSelectedLanguageCode, languageCode);
  prefs.setString(prefSelectedCountryCode, countryCode);
  Get.locale = _locale(languageCode, countryCode);
}


Future<Locale> getLocale() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String languageCode = prefs.getString(prefSelectedLanguageCode) ?? "th";
  String countryCode = prefs.getString(prefSelectedCountryCode) ?? "TH";
  return _locale(languageCode, countryCode);
}

Locale _locale(String languageCode,String countryCode) {
  return languageCode.isNotEmpty
      ? Locale(languageCode, countryCode)
      : defaultLocale;
}

class LocaleInfo {
  final String title;
  final Locale locale;
  const LocaleInfo(this.title,this.locale);
}