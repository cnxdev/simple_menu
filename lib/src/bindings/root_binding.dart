import 'package:get/get.dart';

import '../controllers/app_controller.dart';
import '../../frameworks/translations/locale_constant.dart';

class RootBinding implements Bindings {
  @override
  void dependencies() {

    getLocale().then((locale) {
      Get.updateLocale(locale);
    });
    Get.put(AppController());
  }
}