import 'package:flutter/material.dart';

const kTitleApp =  "Charoen Bar";

const kPrimaryColor = Color(0xffff974d);
const kTextColor = Color(0xFF202E2E);
const kTextLightColor = Color(0xFF7286A5);