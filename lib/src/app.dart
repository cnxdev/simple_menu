import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:menu/src/screen/detail/detail.dart';
import 'package:menu/src/screen/home/home.dart';
import 'package:menu/src/translations/messages.dart';

import 'bindings/root_binding.dart';

class AppScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}

class Application extends StatelessWidget {
  const Application({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      scrollBehavior: AppScrollBehavior(),
      initialBinding: RootBinding(),
      title: 'Charoen Bar',
      translations: Messages(),
      locale: Get.locale,
      fallbackLocale: AppLanguage.thai.localeInfo.locale,
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
      //home:  const Counter(),
    );
  }
}