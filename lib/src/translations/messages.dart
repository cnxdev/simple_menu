import 'dart:ui';

import 'package:get/get.dart';
import '../../frameworks/translations/locale_constant.dart';

enum AppLanguage {
  thai,english
}

extension AppLanguageExtension on AppLanguage {
  LocaleInfo get localeInfo {
    switch(this) {
      case AppLanguage.thai:
        return const LocaleInfo('ภาษาไทย', Locale('th','TH'));
      default:
        return const LocaleInfo('English', Locale('en','US'));
    }

  }

  String get tag {
    switch(this) {
      case AppLanguage.thai:
        return  'th_TH';
      default:
        return 'en_US';
    }

  }
}


class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
    AppLanguage.english.tag: {
      'appTitle': 'Charoen Bar',
      'chooseLanguage' : 'Choose Language',
      'all': 'All',
      'food' : 'Foods',
      "liquor" : "Liquors",
      "cocktail" : "Cocktails",
      "drink" : "Drinks"
    },
    AppLanguage.thai.tag: {
      'appTitle': 'เจริญบาร์',
      'chooseLanguage' : 'เลือกภาษา',
      'all': 'ทั้งหมด',
      'food' : 'อาหาร',
      "liquor" : "สุรา",
      "cocktail" : "ค๊อกเทล",
      "drink" : "เครื่องดื่ม"
    }
  };
}