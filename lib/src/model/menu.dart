import 'dart:convert';

import 'package:flutter/services.dart';
import '../../frameworks/utils/logs.dart';


Menu menuFromJson(String str) => Menu.fromJson(json.decode(str));

String menuToJson(Menu data) => json.encode(data.toJson());

Future<Menu> readMenuFromJsonFile(String fileName) async {
  final String response = await rootBundle.loadString('assets/json/$fileName.json');
  return menuFromJson(response);
}

class Menu {
  Menu({
    required this.categories,
    required this.items,
  });

  List<String> categories;
  List<FoodMenuItem> items;

  factory Menu.fromJson(Map<String, dynamic> json) => Menu(
    categories: List<String>.from(json["categories"].map((x) => x)),
    items: List<FoodMenuItem>.from(json["items"].map((x) => FoodMenuItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories": List<dynamic>.from(categories.map((x) => x)),
    "items": List<dynamic>.from(items.map((x) => x.toJson())),
  };
}

class FoodMenuItem {
  FoodMenuItem({
    required this.image,
    this.title = '',
    required this.type,
  });

  String image;
  String title;
  String type;

  factory FoodMenuItem.fromJson(Map<String, dynamic> json) => FoodMenuItem(
    image: json["image"],
    title: json["title"] ?? '',
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "image": image,
    "title": title,
    "type": type,
  };
}


class CategoryMenuItem {
  final String title;
  final String type;
  final Function pressed;
  CategoryMenuItem({
    required this.title ,
    required this.type,
    required this.pressed
  });
}

