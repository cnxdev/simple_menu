import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:menu/frameworks/utils/logs.dart';

import '../model/menu.dart';

class AppController extends GetxController {


  Menu? _menu;
  var menuItems = [].obs;
  var selectedIndex = 0.obs;
  //var menuItems = [];

  @override
  void onInit() {
    initMenu();
    super.onInit();
  }

  var menuSelected = 0.obs;

  Future initMenu() async {
    _menu = await readMenuFromJsonFile('menu');
    menuItemUpdate();
    _currentMenuItems = getFoodMenuItems();
    //menuItems.value = items;
    //update();
  }

  setSelectedIndex(int index) {
    selectedIndex.value = index;
  }

  incrementSelectedIndex({bool inc = true}) {
    var newVal = selectedIndex.value + (inc ? 1 : -1);
    if (newVal < 0) {
      newVal = 0;
    }
    if (newVal >= _currentMenuItems.length) {
      newVal = _currentMenuItems.length-1;
    }
    setSelectedIndex(newVal);
  }


  FoodMenuItem get selectedItem {
    var items = _currentMenuItems;
    var index = selectedIndex.value;
    if (index < 0) {
      index = 0;
      setSelectedIndex(index);
    }

    if (index >= items.length) {
      index = items.length-1;
      setSelectedIndex(index);
    }

    return items[index];
  }

  List<FoodMenuItem> _currentMenuItems = [];
  List<FoodMenuItem> get currentMenuItems => _currentMenuItems;

  String get itemRange => '${_menu?.categories[menuSelected.value].tr} ${selectedIndex.value+1}/${_currentMenuItems.length}';

  String get currentCategory => _menu?.categories[menuSelected.value].tr ?? '';

  List<ImageProvider> get categoryImages =>
      List.generate(_currentMenuItems.length, (index) => Image.asset('assets/images/${_currentMenuItems[index].image}').image );


  List<FoodMenuItem> getFoodMenuItems() => _currentMenuItems = _menu == null ? [] :
    menuSelected.value == 0 ? _menu!.items :
    _menu!.items.where((item) => item.type == _menu!.categories[menuSelected.value]).toList();

  void menuItemUpdate() {
    var menu = _menu!;
    var items = List.generate(
        menu.categories.length, (index) =>
        CategoryMenuItem(
            title: menu.categories[index].tr ,
            type: menu.categories[index]  ,
            pressed: () {
              menuSelected.value = index;
              selectedIndex.value = 0;
              //_currentMenuItems = getFoodMenuItems();


              //update();
            }
        )
    );
    menuItems.value = items;
    printLog('Menu Item update');
  }

  bool isActiveIndex(int index) => index == menuSelected.value;

  //var count = 0.obs;
  //increment() => count++;
}