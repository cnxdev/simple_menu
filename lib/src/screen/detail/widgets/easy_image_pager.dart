import 'package:flutter/cupertino.dart';

class EasyImageViewPager extends StatefulWidget {

  final List<ImageProvider> imageProviders;
  final int initPage;
  final void Function(int)? onPageChanged;

  /// Create new instance, using the [imageProviders] to populate the [PageView]
  const EasyImageViewPager({
    Key? key,
    required this.imageProviders ,
    this.initPage = 0,
    this.onPageChanged
  }) : super(key: key);

  @override
  EasyImageViewPagerState createState() => EasyImageViewPagerState();
}

class EasyImageViewPagerState extends State<EasyImageViewPager> {

  late PageController _pageController;
  bool _pagingEnabled = true;

  @override
  void initState() {
    _pageController = PageController(initialPage: widget.initPage);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: _pagingEnabled ? const PageScrollPhysics() : const NeverScrollableScrollPhysics(),
      itemCount: widget.imageProviders.length,
      controller: _pageController,
      onPageChanged: widget.onPageChanged,
      itemBuilder: (context, index) {
        final image = widget.imageProviders[index];
        return EasyImageView(
          imageProvider: image,
          onScaleChanged: (scale) {
            setState(() {
              // Disable paging when image is zoomed-in
              _pagingEnabled = scale <= 1.0;
            });
          },
        );
      },
    );
  }
}

/// A full-sized view that displays the given image, supporting pinch & zoom
class EasyImageView extends StatefulWidget {

  /// The image to display
  final ImageProvider imageProvider;
  /// Minimum scale factor
  final double minScale;
  /// Maximum scale factor
  final double maxScale;
  /// Callback for when the scale has changed, only invoked at the end of
  /// an interaction.
  final void Function(double)? onScaleChanged;
  /// Create a new instance
  const EasyImageView({
    Key? key,
    required this.imageProvider,
    this.minScale = 1.0,
    this.maxScale = 5.0,
    this.onScaleChanged,
  }) : super(key: key);

  @override
  EasyImageViewState createState() => EasyImageViewState();
}

class EasyImageViewState extends State<EasyImageView> {

  final TransformationController _transformationController = TransformationController();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: InteractiveViewer(
          transformationController: _transformationController,
          minScale: widget.minScale,
          maxScale: widget.maxScale,
          child: Image(image: widget.imageProvider),
          onInteractionEnd: (scaleEndDetails) {
            double scale = _transformationController.value.getMaxScaleOnAxis();

            if (widget.onScaleChanged != null) {
              widget.onScaleChanged!(scale);
            }
          },
        )
    );
  }
}