import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:menu/frameworks/utils/logs.dart';

import '../../controllers/app_controller.dart';
import '../../model/menu.dart';
import 'widgets/easy_image_pager.dart';

class CategoriesPageView extends StatelessWidget {
  const CategoriesPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AppController controller = Get.find();
    var size = MediaQuery.of(context).size;
    var iconSize = size.width*0.075;

    content() =>  Row(
      children: [
        Text(
          controller.itemRange,
          style: Theme.of(context).textTheme.headline3?.copyWith(color: Colors.red.shade400 , fontSize: iconSize*0.7),
        ),
        const SizedBox(width: 30,),
        MaterialButton(
          onPressed: () {
            Get.back();
          },
          height: iconSize,
          minWidth: iconSize,
          color: Colors.white.withOpacity(0.3),
          textColor: Colors.red,
          padding: const EdgeInsets.all(2),
          shape: const CircleBorder(),
          child: Icon(Icons.close_rounded,color: Colors.red,size: iconSize,),
        )
      ],
    );

    return SafeArea(
      child: Stack(
          children: [
            EasyImageViewPager(
              imageProviders: controller.categoryImages ,
              initPage: controller.selectedIndex.value,
              onPageChanged: (index) => controller.setSelectedIndex(index),
            ),
            Positioned(
              top: 18,
              right: 8,
              child: Obx(() => content()),
            ),
          ]
      )
    );
  }
}


class FullScreenMenu extends StatelessWidget {
  const FullScreenMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AppController controller = Get.find();
    var size = MediaQuery.of(context).size;
    var iconSize = size.width*0.075;

    content(FoodMenuItem item) => Stack(
      children: [
        Container(
          width: size.width,
          height: size.height,
          color: Colors.black,
          child: InteractiveViewer(
            minScale: 1.0,
            maxScale: 4.0,
            child: Image.asset('assets/images/${item.image}',width: size.width,fit: BoxFit.fitWidth,),
          ),
        ),
        Positioned(
            top: 18,
            right: 8,
            child: Row(
              children: [
                IconButton(onPressed: ()  {
                  controller.incrementSelectedIndex(inc: false);
                } ,
                    icon:  Icon( Icons.arrow_left,color: Colors.orange,size: iconSize ,)
                ),
                Text(
                  controller.itemRange,
                  style: Theme.of(context).textTheme.headline3?.copyWith(color: Colors.red.shade400 , fontSize: iconSize*0.7),
                ),
                IconButton(onPressed: ()  {
                  controller.incrementSelectedIndex();
                } ,
                    icon:  Icon( Icons.arrow_right,color: Colors.orange,size: iconSize ,)
                ),
                const SizedBox(width: 30,),
                MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  height: iconSize,
                  minWidth: iconSize,
                  color: Colors.white.withOpacity(0.3),
                  textColor: Colors.red,
                  padding: const EdgeInsets.all(2),
                  shape: const CircleBorder(),
                  child: Icon(Icons.close_rounded,color: Colors.red,size: iconSize,),
                )
              ],
            )
        )
      ],
    );

    return Scaffold(
      body: SafeArea(
        child: Obx(() {
          return GestureDetector(
            onPanUpdate: (detail) {
              printLog('detail ${detail.delta}');
              //controller.incrementSelectedIndex(inc: false);
            },
            onHorizontalDragEnd: (details) => controller.incrementSelectedIndex(inc: details.primaryVelocity! < 0.0),
            child: content(controller.selectedItem),
          );
          return content(controller.selectedItem);
        })
      ),
    );
  }
}


class FoodDetail extends StatelessWidget {
  final FoodMenuItem item;
  const FoodDetail({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var iconSize = size.width*0.075;

    return Scaffold(
      body: SafeArea(child:
        Stack(
          children: [
            Container(
              width: size.width,
              height: size.height,
              color: Colors.black,
              child: InteractiveViewer(
                minScale: 1.0,
                maxScale: 4.0,
                child: Image.asset('assets/images/${item.image}',width: size.width,fit: BoxFit.fitWidth,),
              ),
            ),
            Positioned(
                top: 18,
                right: 8,
                child: Row(
                  children: [
                    const SizedBox(width: 5,),
                    MaterialButton(
                      onPressed: () {
                        Get.back();
                      },
                      height: iconSize,
                      minWidth: iconSize,
                      color: Colors.white.withOpacity(0.3),
                      textColor: Colors.red,
                      padding: const EdgeInsets.all(2),
                      shape: const CircleBorder(),
                      child: Icon(Icons.close_rounded,color: Colors.red,size: iconSize,),
                    )
                  ],
                )
            )
          ],
        ),
      ),
    );
  }
}
