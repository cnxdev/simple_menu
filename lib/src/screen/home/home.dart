import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:menu/src/screen/home/widgets/horizon_top_menu.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../../../frameworks/utils/logs.dart';
import '../../constant.dart';
import '../../controllers/app_controller.dart';
import '../../model/menu.dart';
import 'widgets/vertical_side_menu.dart';
import 'widgets/food_item.dart';
import '../../translations/messages.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(
        child: BodyHome(),
      ),
    );
  }
}

class BodyHome extends StatelessWidget {
  const BodyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)  {

    final AppController controller = Get.find();
    final menuItems = controller.menuItems;


    buildSideMenu() {//({double width = 60}) {
      return SizedBox(width: 60,
        child: menuItems.isEmpty ? null :
        Container(
          padding: const EdgeInsets.symmetric(vertical: 9.0),
          height: double.infinity,
          decoration: const BoxDecoration(
            color: kPrimaryColor,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(25.0),
              bottomRight: Radius.circular(25.0),
            ),
          ),
          child: Column(
            children:  <Widget>[
              const SizedBox(height: 50,),
              //IconButton(icon: const Icon(Icons.apps), onPressed: () {}),
              //IconButton(icon: const Icon(Icons.search), onPressed: () {}),
              Expanded(
                  child:   VerticalSideMenu(controller: controller,)
              ),
              //IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {}),
            ],
          ),
        ),
      );
    }

    buildContents() {
      //var items = controller.foodMenuItems();
      //return GalleryPhotoViewWrapper(galleryItems: items,);

      return ListView(
        children: itemWidgets(controller.getFoodMenuItems()),
      );
    }

    buildBody() {
      return Column(
        children: [

          SizedBox(
            //width: double.infinity,
            height: 45,
            child: Padding(
              padding: EdgeInsets.only(top: 10 , bottom: 5,left: 10,right: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('appTitle'.tr, style: TextStyle( fontWeight: FontWeight.bold , fontSize: 20 , color: kPrimaryColor ),),
                  ToggleSwitch(
                    minWidth: 50.0,
                    cornerRadius: 20.0,
                    activeBgColors: [[Colors.green[800]!], [Colors.red[800]!]],
                    activeFgColor: Colors.white,
                    inactiveBgColor: Colors.grey,
                    inactiveFgColor: Colors.white,
                    initialLabelIndex: Get.locale?.languageCode == 'th' ? 0 : 1 ,
                    totalSwitches: 2,
                    labels: const ['ไทย', 'Eng'],
                    radiusStyle: true,
                    onToggle: (index) {
                      Get.updateLocale(Locale(index == 0 ? 'th' : 'en' ));
                      controller.menuItemUpdate();
                    },
                  ),
                ],
              ),
            ),
            //child: Image.asset("assets/images/logo.png" , alignment: Alignment.topLeft,),
          ),
          SizedBox(
            //width: double.infinity,
              height: 60,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: HorizontalTopMenu(controller: controller,),
              )
          ),
          //_buildTopMenu(context),
          Expanded(
            //child: buildContents(context),
            child: buildContents(),
          ),
          const SizedBox(
            width: double.infinity,
            height: 40,
            //child: Image.asset("assets/images/logo.png" , alignment: Alignment.topLeft,),
          ),
        ],
      );
    }




    return Obx(() {
      return Row(
        children:  [
          buildSideMenu(),
          Expanded(
            child: buildBody(),
            //child: Text('${controller.menuSelected}')
          ),
        ],
      );
    });
  }
}



class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex = 0,
    required this.galleryItems,
    this.scrollDirection = Axis.horizontal,
  }) : pageController = PageController(initialPage: initialIndex);

  final LoadingBuilder? loadingBuilder;
  final BoxDecoration? backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final PageController pageController;
  final List<FoodMenuItem> galleryItems;
  final Axis scrollDirection;

  @override
  State<StatefulWidget> createState() {
    return _GalleryPhotoViewWrapperState();
  }
}

class _GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  late int currentIndex = widget.initialIndex;

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: _buildItem,
              itemCount: widget.galleryItems.length,
              loadingBuilder: widget.loadingBuilder,
              backgroundDecoration: widget.backgroundDecoration,
              pageController: widget.pageController,
              onPageChanged: onPageChanged,
              scrollDirection: widget.scrollDirection,
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "Image ${currentIndex + 1}",
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                  decoration: null,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final FoodMenuItem item = widget.galleryItems[index];
    return PhotoViewGalleryPageOptions(
      imageProvider: AssetImage('assets/images/${item.image}'),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 4.1,
      //heroAttributes: PhotoViewHeroAttributes(tag: item.id),
    );
  }
}