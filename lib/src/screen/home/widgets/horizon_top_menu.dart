import 'package:flutter/material.dart';

import '../../../controllers/app_controller.dart';

class HorizontalTopMenu extends StatelessWidget {

  final Color bodyColor,activeBodyColor;
  final Color textColor,activeTextColor;
  final AppController controller;

  const HorizontalTopMenu({
    Key? key,
    this.bodyColor = Colors.white,
    this.activeBodyColor = Colors.deepOrangeAccent,
    this.textColor = Colors.black,
    this.activeTextColor = Colors.white,
    required this.controller
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    var menuItems = controller.menuItems;

    return ListView.builder(
      itemCount: menuItems.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) => Center(
        child: GestureDetector(
          onTap: () {
            menuItems[i].pressed();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 9,
            ),
            margin: const EdgeInsets.only(right: 9),
            decoration: BoxDecoration(
              color: controller.isActiveIndex(i) ? activeBodyColor : bodyColor,
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(15.0),
                bottomLeft: Radius.circular(15.0),
              ),
            ),
            child: Text(
              menuItems[i].title,
              style: Theme.of(context).textTheme.headline6?.copyWith(
                color: controller.isActiveIndex(i) ?  activeTextColor : textColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
/*
import '../../../model/menu.dart';

class TopMenuItem {
  final String title;
  final Function pressed;
  TopMenuItem({
    required this.title ,
    required this.pressed
  });
}


class HorizontalTopMenu extends StatefulWidget {
  final List<CategoryMenuItem> _topMenuItems;
  final Color bodyColor,activeBodyColor;
  final Color textColor,activeTextColor;
  const HorizontalTopMenu(this._topMenuItems,{
    Key? key,
    required this.bodyColor,
    this.activeBodyColor = Colors.deepOrangeAccent,
    required this.textColor,
    this.activeTextColor = Colors.white

  }) : super(key: key);

  @override
  HorizontalTopMenuState createState() => HorizontalTopMenuState();
}

class HorizontalTopMenuState extends State<HorizontalTopMenu> {
  int _active = 0;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget._topMenuItems.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, i) => Center(
        child: GestureDetector(
          onTap: () {
            widget._topMenuItems[i].pressed();
            setState(() {
              _active = i;
            });
          },
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 9,
            ),
            margin: const EdgeInsets.only(right: 9),
            decoration: BoxDecoration(
              color: i == _active ? widget.activeBodyColor : widget.bodyColor,
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(15.0),
                bottomLeft: Radius.circular(15.0),
              ),
            ),
            child: Text(
              widget._topMenuItems[i].title,
              style: Theme.of(context).textTheme.headline6?.copyWith(
                color: i == _active ? widget.activeTextColor : widget.textColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  setActive(int index) {
    if (_active != index) {
      setState(() {
        _active = index;
      });
    }
  }
}
*/