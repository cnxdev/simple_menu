
import 'package:flutter/material.dart';
import '../../../controllers/app_controller.dart';


class VerticalSideMenu extends StatelessWidget {

  final Color dotColor;
  final Color textColor,activeTextColor;
  final AppController controller;

   const VerticalSideMenu({
    Key? key,
    this.dotColor = Colors.black,
    this.textColor = Colors.black,
    this.activeTextColor = Colors.white,
    required this.controller
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {

    var menuItems = controller.menuItems;
    //final AppController controller = Get.find();

    return ListView.builder(
      itemCount: menuItems.length,
      itemBuilder: (context, i) => GestureDetector(
        onTap: () {
          menuItems[i].pressed();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(vertical: 9),
              child: RotatedBox(
                quarterTurns: -1,
                child: Text(menuItems[i].title,
                    style: Theme.of(context).textTheme.headline4?.copyWith(
                      color: controller.isActiveIndex(i) ? activeTextColor : textColor,
                    )),
              ),
            ),
            if (controller.isActiveIndex(i)) ...[
              const SizedBox(width: 3),
              Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                  color: dotColor,
                  shape: BoxShape.circle,
                ),
              )
            ]
          ],
        ),
      ),
    );
  }
}