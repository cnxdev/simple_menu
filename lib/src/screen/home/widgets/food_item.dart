import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:menu/src/screen/detail/detail.dart';
import '../../../controllers/app_controller.dart';
import '../../../model/menu.dart';



class ItemWidget extends StatelessWidget {
  final FoodMenuItem item;
  final int index;
  final double height;
  final Color contentColor;
  const ItemWidget({
    Key? key,
    required this.item,
    required this.index,
    this.height = 0,
    this.contentColor = Colors.black
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final AppController controller = Get.find();

    return GestureDetector(
      onTap: () {
        controller.setSelectedIndex(index);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>  CategoriesPageView()));
        //Navigator.push(context, MaterialPageRoute(builder: (context)=>FoodDetail(item: item)));
      },
      child: Container(
        color: contentColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5 , horizontal: 10),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.asset('assets/images/${item.image}',width: size.width*0.75,fit: BoxFit.fitWidth,),
              /*
              child: InteractiveViewer(
                minScale: 1.0,
                maxScale: 4.0,
                child: Image.asset('assets/images/${item.image}',width: size.width*0.75,fit: BoxFit.fitWidth,),
              )*/
          ),
        ),
      ),
    );
  }
}

List<ItemWidget> itemWidgets(List<FoodMenuItem> items) {
  return List.generate(items.length, (index) => ItemWidget(item: items[index],index: index,));
}
