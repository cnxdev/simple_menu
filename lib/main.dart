import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:menu/src/bindings/root_binding.dart';
import 'package:menu/src/controllers/app_controller.dart';
import 'package:menu/src/translations/messages.dart';

import 'src/app.dart';

//import 'src/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const Application());
}
